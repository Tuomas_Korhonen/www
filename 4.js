/* Tekij�: Tuomas Korhonen
Opiskelijanumero: 0453540
P�iv�m��r�: 14.10.2018 */


$(document).ready(function() {
  $('input#input_text, textarea#textarea2').characterCounter();
});

Vue.component("item", {
    props: ["name"],
    template: `<span v-html="name"></span>`
})
var app = new Vue({
  el: "#app",
  name: 'vue-instance',

  data: {
      newItem: "",
      Kalevi: "Kalevi",
      items: ["kaljoo"],
      errors: [],
  },
  mounted() {
      if (localStorage.getItem('items')) {
        try {
          this.items = JSON.parse(localStorage.getItem('items'));
        }
        catch(e) {
          localStorage.removeItem('items');
        }
      }
    },

  methods: {
    removeItem(index) {
      this.items.splice(index,1);
      this.saveItems();
    },

    add() {
      if (this.newItem === '') {
        return;
      }
      var re = /^[a-zA-Z1-9äÄöÖ,.]+$/;
      if (re.test(this.newItem) && this.newItem.length < 20) {
        this.items.push(this.newItem);
        this.newItem = '';
        this.saveItems();
      }
    },


    saveItems() {
      const parsed = JSON.stringify(this.items);
      localStorage.setItem('items', parsed);
    },
  },
});
